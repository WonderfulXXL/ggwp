﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Photon.Realtime;

public class PlayerController : MonoBehaviourPunCallbacks , IDamageble
{
    [SerializeField] GameObject cameraHolder;

    [SerializeField] float mouseSensitive, spritSpeed, walkSpeed, jumpForce, smoothTime;

    [SerializeField] Item[] items;

    int itemIndex;
    int previousItemIndex = -1;

    float verticalLookRotation;
   public bool grounded;
    Vector3 smoothMoveVelocity;
    Vector3 moveAmount;

    Rigidbody rb;
    PhotonView PV;

    const float maxHealth = 100f;
    float currentHealth = maxHealth;

    PlayerManager playerManager;

   void Awake()
    {
        rb = GetComponent<Rigidbody>();
        PV = GetComponent<PhotonView>();
        playerManager = PhotonView.Find((int)PV.InstantiationData[0]).GetComponent<PlayerManager>();
    }
   void Update()
    {
        if (!PV.IsMine)
            return;
        Look();
        Move();
        Jump();

        for(int i = 0; i < items.Length;i++)
        {
            if(Input.GetKeyDown((i+1).ToString()))
            {
                EquipItem(i);
                break;
            }
        }
        if(Input.GetMouseButtonDown(0))
        {
            items[itemIndex].Use();
        }
      /*  if (Input.GetAxisRaw("Mouse ScrollWheel") > 0 )
        {
            EquipItem(itemIndex + 1);

        }
        else if (Input.GetAxisRaw("Mouse ScrollWheel") < 0)
        {
            EquipItem(itemIndex - 1);
        }*/
    }
    
     void Start()
    {
        if(PV.IsMine)
        {
            EquipItem(0);
        }
        else
        {
            Destroy(GetComponentInChildren<Camera>().gameObject);
            Destroy(rb);
        }
    }

    void Look()
    {
            transform.Rotate(Vector3.up * Input.GetAxisRaw("Mouse X") * mouseSensitive);
            verticalLookRotation += Input.GetAxisRaw("Mouse Y") * mouseSensitive;
            verticalLookRotation = Mathf.Clamp(verticalLookRotation, -90f, 90f);

            cameraHolder.transform.localEulerAngles = Vector3.left * verticalLookRotation;
       
    }
    void Move()
    {
        Vector3 moveDir = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized;

        moveAmount = Vector3.SmoothDamp(moveAmount, moveDir * (Input.GetKey(KeyCode.LeftShift) ? spritSpeed : walkSpeed), ref smoothMoveVelocity, smoothTime);
    }
    void Jump()
    {

        if (Input.GetKeyDown(KeyCode.Space) && grounded)
        {
            rb.AddForce(transform.up * jumpForce);
        }  
    }
    void EquipItem(int _index)
    {
        if (_index == previousItemIndex)
            return;
        itemIndex = _index;

        items[itemIndex].itemGameObject.SetActive(true);

        if(previousItemIndex != -1)
        {
            items[previousItemIndex].itemGameObject.SetActive(false);
        }
        previousItemIndex = itemIndex;

        if(PV.IsMine)
        {
            Hashtable hash = new Hashtable();
            hash.Add("itemIndex", itemIndex);
            PhotonNetwork.LocalPlayer.SetCustomProperties(hash);
        }
    }
    public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
    {
        if(!PV.IsMine && targetPlayer == PV.Owner)
        EquipItem((int)changedProps["itemIndex"]);
    }

    public void SetGroundedState(bool _grounded)
    {
        grounded = _grounded;
    }
   void FixedUpdate()
    {
        if (!PV.IsMine)
            return;
        rb.MovePosition(rb.position + transform.TransformDirection(moveAmount) * Time.fixedDeltaTime);
    }
    public void TakeDamage(float damage)

    {
        PV.RPC("RPC_TakeDamage", RpcTarget.All, damage);
    }

    [PunRPC]
    void RPC_TakeDamage(float damage)
    {
        if (!PV.IsMine)
            return;
        Debug.Log("Huy : " + damage);
        currentHealth -= damage;
        if(currentHealth <0)
        {
            Die();
        }
    }
    void Die()
    {
        playerManager.Die();
    }
}
 